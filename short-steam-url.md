Url | What
--- | ----
https://s.team/a/440         | App store page
https://s.team/c/440         | App community page
https://s.team/u/xpaw        | Profile page (vanity url)
https://s.team/p/qpn-pmn     | Profile page (account id)
https://s.team/chat/DpOLGWbt | Steam chat invite
https://s.team/y23/qpnpmn    | Steam replay
https://s.team/q/123         | Steam login QR code link
https://s.team/deck          | HelpWithSteamDeck
https://s.team/hw_help       | HelpWithSteamHardware
https://s.team/join          | New account
https://s.team/remoteplay/connect/?appid=440 | Remote play
